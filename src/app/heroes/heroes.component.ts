import { Component, OnInit } from '@angular/core';

import { Hero } from '../hero'; 
import { HEROES } from '../mock-heroes'; 
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  // heroes = HEROES;
  // heroes : Hero[] | undefined;
  heroes : Hero[] = [];

  // constructor() { }
  constructor(private heroService: HeroService, private messageService: MessageService) {}

  // ngOnInit(): void {
  // }
  ngOnInit() {
    this.getHeroes();
  }
  // atributo para borrar, este metodo tenia funcionalidad cuando se hacia un select sin redireccion
  // selectedHero: Hero | undefined;
  // metodo para borrar, este metodo tenia funcionalidad cuando se hacia un select sin redireccion
  // onSelect(hero: Hero): void {
  //   this.selectedHero = hero;
  //   this.messageService.add(`HeroesComponent: Selected hero id=${hero.id}`);
  // }

  // metodo cuando era sincronico 
  // getHeroes(): void {
  //   this.heroes = this.heroService.getHeroes();
  // } 
  
  // metodo para asincronico 
  getHeroes(): void {
    this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero.id).subscribe();
  }

}
